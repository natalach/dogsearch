# Dogsearch
Welcome to [Dogsearch]  - a simple app that allows you to browse photos of dogs from Flickr.

# Features
The way this app works is very simple - it displays a list of 100 newest photos with tag 'dogs' from Flickr. The user can always fetch 100 more at the bottom of the page. 

The pictures are displayed in 5 columns by default. It is possible to change it to 2 or 3 and see bigger images by choosing it on the top right corner.

To see a modal with details about a single picture as well as its biger version, one should click the photo he is interested in.

The photos might be added to favourites by clicking a Star icon displayed at the top left corner of each one or a button on the single photo view. The same applies for deleting a picture from favourites.

The list of all favourite photos might be seen after clicking button 'Show favourites' at the top left corner of the page. If there is nothing to show, a corresponding message is displayed. 

Another function of this app is search bar which allows to look for photos of dogs with a certain description. If one isn't interested in this description any more, there is a button restoring search parameters to the basic ones. 

In case of error fetching a photo or searching (when there is no result), an alert is displayed on the screen.

# About the project:

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To run it, please create a .env file and paste yout Flickr API key first.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).



[Dogsearch]: <https://dogsearch.now.sh/>