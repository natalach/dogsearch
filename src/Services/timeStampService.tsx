export const convertTimestamp = (secs: number) => {
  const t = new Date(secs);
  return t.toString();
};
