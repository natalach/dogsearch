import Photo from "../Interfaces/Photo";

export const init = () => {
  if (localStorage.getItem("favouritesList") === null) {
    const listOfOavourites: Photo[] = [];
    const stringified = JSON.stringify(listOfOavourites);
    localStorage.setItem("favouritesList", stringified);
  }
};

export const getFavPhotos = () => {
  const parsed = localStorage.getItem("favouritesList") || "[]";
  return JSON.parse(parsed);
};

function modifyArrayofLikedPhotos(currentArray: Photo[], photo: Photo) {
  if (currentArray.some(obj => obj.id === photo.id)) {
    return currentArray.filter(obj => {
      return obj.id !== photo.id;
    });
  } else {
    currentArray.push(photo);
    return currentArray;
  }
}

export const updateLocalStorage = (photo: Photo) => {
  const list = getFavPhotos();
  const likedPhotos = modifyArrayofLikedPhotos(list, photo);
  localStorage.setItem("favouritesList", JSON.stringify(likedPhotos));
};

export const checkIfLiked = (photo: Photo) => {
  const list: Photo[] = getFavPhotos();
  return list.some(obj => obj.id === photo.id) ? true : false;
};
