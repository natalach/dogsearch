import React, { useState, useCallback, useContext } from "react";

import {
  Button,
  Card,
  CardActions,
  CardContent,
  IconButton,
  Typography
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

import Loading from "./Loading";
import Photo from "../Interfaces/Photo";
import { ListContext } from "../Context/listContext";
import { convertTimestamp } from "../Services/timeStampService";
import useStyles from "../Hooks/useStyles";

interface Props {
  photo: Photo;
  loading: boolean;
  photoData: any;
  photoUrl: string;

  isLiked: boolean;
}

const SinglePhoto: React.FC<Props> = ({
  photo,
  loading,
  photoData,
  photoUrl,
  isLiked
}) => {
  const { onXClicked, onLikeClicked } = useContext(ListContext);
  const [imgWidth, setImgWidth] = useState(0);
  const [imgHeight, setImgHeight] = useState(0);
  const [imgLoading, setImgLoading] = useState<boolean>(true);

  const horizontal = {
    width: "90%",
    marginTop: "15px"
  };
  const vertical = {
    width: "60%",
    marginTop: "15px"
  };
  const styles = () => {
    if (imgWidth > imgHeight) return horizontal;
    else return vertical;
  };
  const likeButtonText = isLiked
    ? "Remove from favourites"
    : "Add to favourites";

  const classes = useStyles();

  const onLoad = useCallback((e: React.ChangeEvent<HTMLImageElement>) => {
    setImgLoading(false);
    setImgWidth(e.target.naturalWidth);
    setImgHeight(e.target.naturalHeight);
  }, []);

  const onError = useCallback(
    (e: React.SyntheticEvent<HTMLImageElement, Event>) => {
      alert("Error fetching this photo");
      console.log("error");
      console.log(e);
      setImgLoading(false);
    },
    []
  );

  return (
    <div>
      {loading ? (
        <div>Loading...</div>
      ) : (
        <Card className={classes.card}>
          <div style={{ textAlign: "right" }}>
            <IconButton aria-label={`star`} onClick={onXClicked}>
              <CloseIcon />
            </IconButton>
          </div>
          {imgLoading && <div>Loading...</div>}
          <img
            className={classes.image}
            style={styles()}
            src={photoUrl}
            onLoad={onLoad}
            onError={onError}
            alt="image"
          />
          {photoData ? (
            <div>
              {" "}
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  {photo.title ? photo.title : "Untitled"}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="textSecondary"
                  component="p"
                >
                  Author:{" "}
                  {photoData.photo.owner.username
                    ? photoData.photo.owner.username
                    : ""}{" "}
                  {photoData.photo.owner.realname
                    ? "(" + photoData.photo.owner.realname + ")"
                    : ""}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {photoData.photo.description._content
                    ? photoData.photo.description._content
                    : "No description"}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {photoData.photo.dates.taken
                    ? "taken: " + photoData.photo.dates.taken + " "
                    : ""}{" "}
                  {photoData.photo.dates.posted
                    ? "posted: " +
                      convertTimestamp(parseInt(photoData.photo.dates.posted)) +
                      ")"
                    : ""}
                </Typography>
              </CardContent>{" "}
              <CardActions>
                <Button
                  size="small"
                  color="primary"
                  href={
                    "https://www.flickr.com/photos/" +
                    photoData.photo.owner.nsid
                  }
                >
                  Visit author's profile
                </Button>
                <Button
                  size="small"
                  color="primary"
                  href={
                    "https://www.flickr.com/photos/" +
                    photoData.photo.owner.nsid +
                    "/" +
                    photo.id
                  }
                >
                  See photo on Flickr
                </Button>
                <Button
                  onClick={e => onLikeClicked(e, photo)}
                  size="small"
                  color="primary"
                >
                  {likeButtonText}
                </Button>
              </CardActions>{" "}
            </div>
          ) : (
            <Loading />
          )}
        </Card>
      )}
    </div>
  );
};
export default SinglePhoto;
