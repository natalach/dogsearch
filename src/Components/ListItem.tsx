import React, { useState, useCallback, useContext } from "react";
import GridListTileBar from "@material-ui/core/GridListTileBar";

import IconButton from "@material-ui/core/IconButton";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import StarIcon from "@material-ui/icons/Star";

import Loading from "./Loading";
import useStyles from "../Hooks/useStyles";
import Photo from "../Interfaces/Photo";
import { ListContext } from "../Context/listContext";

interface Props {
  photo: Photo;
  isLiked: boolean;
}

const ListItem: React.FC<Props> = ({ isLiked, photo }) => {
  const { onLikeClicked, onPhotoClicked } = useContext(ListContext);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [imgWidth, setImgWidth] = useState(0);
  const [imgHeight, setImgHeight] = useState(0);
  const photoUrl: string =
    "https://farm" +
    photo.farm +
    ".staticflickr.com/" +
    photo.server +
    "/" +
    photo.id +
    "_" +
    photo.secret +
    ".jpg";
  const handleError = useCallback(
    (e: React.SyntheticEvent<HTMLImageElement, Event>) => {
      setIsLoaded(true);
      alert("Error fetching this photo");
      console.log("error");
      console.log(e);
    },
    []
  );

  const onLoad = useCallback((e: React.ChangeEvent<HTMLImageElement>) => {
    setIsLoaded(true);
    setImgWidth(e.target.naturalWidth);
    setImgHeight(e.target.naturalHeight);
  }, []);
  const classes = useStyles();

  const styles = () => {
    if (imgWidth > imgHeight) return classes.horizontal;
    else return classes.vertical;
  };

  return (
    <div className="photo">
      {isLoaded ? <div> </div> : <Loading />}
      <img
        className={styles()}
        src={photoUrl}
        alt=""
        onClick={e => onPhotoClicked(e, photo)}
        onError={e => handleError(e)}
        onLoad={onLoad}
      />
      <GridListTileBar
        titlePosition="top"
        actionIcon={
          <IconButton
            aria-label={`star`}
            className={classes.icon}
            onClick={e => onLikeClicked(e, photo)}
          >
            {isLiked ? <StarIcon /> : <StarBorderIcon />}
          </IconButton>
        }
        actionPosition="left"
        className={classes.titleBar}
      />
    </div>
  );
};

export default ListItem;
