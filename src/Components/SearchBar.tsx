import React, { useState, useContext } from "react";
import { Input } from "@material-ui/core";

import { ListContext } from "../Context/listContext";

interface Props {}

const SearchBar: React.FC<Props> = () => {
  const { onChange, onSubmit } = useContext(ListContext);
  const [val, setVal] = useState<string>("");
  const onInSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    onSubmit(event);
    setVal("");
  };
  const onInChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setVal(event.target.value);
    onChange(event);
  };
  return (
    <div>
      <form onSubmit={onInSubmit}>
        <Input
          id="elementId"
          value={val}
          inputProps={{ "aria-label": "description" }}
          onChange={onInChange}
        />
        <Input type="submit" value="Search" />
      </form>
    </div>
  );
};

export default SearchBar;
