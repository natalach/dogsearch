import React from "react";
import useStyles from "../Hooks/useStyles";

function Loading() {
  const classes = useStyles();
  return <div className={classes.noFav}>Loading...</div>;
}

export default Loading;
