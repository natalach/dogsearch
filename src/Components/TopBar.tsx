import React from "react";
import { Typography } from "@material-ui/core";

const TopBar = () => {
  return (
    <div style={{ textAlign: "center", height: "150px" }}>
      <Typography variant="h1" component="h2">
        Welcome to Doghsearch!
      </Typography>
    </div>
  );
};

export default TopBar;
