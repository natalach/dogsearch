import React, { useState, useContext } from "react";
import { ListContext } from "../Context/listContext";

import {
  Dialog,
  GridList,
  GridListTile,
  DialogContent,
  Typography,
  Button,
  ButtonGroup
} from "@material-ui/core";
import PetsIcon from "@material-ui/icons/Pets";

import ListItem from "./ListItem";
import SearchBar from "./SearchBar";
import SinglePhotoContainer from "../Containers/SinglePhotoContainer";
import Loading from "./Loading";
import useStyles from "../Hooks/useStyles";
import { checkIfLiked } from "../Services/localStorageService";

interface Props {}

const List: React.FC<Props> = () => {
  const [numberOfCols, setNumberOfCols] = useState<number>(5);

  const howManyRows = () => {
    if (numberOfCols === 5) return 2;
    if (numberOfCols === 3) return 3;
    if (numberOfCols === 2) return 5;
  };

  const classes = useStyles();
  const {
    singlePhotoView,
    favPictures,
    photoToShow,
    showFavourites,
    onXClicked,
    switchViewToFavourites,
    onClear,
    loadMorePages,
    loading,
    textToSearch,
    pictures
  } = useContext(ListContext);
  return (
    <div>
      <div>
        {" "}
        <Button
          variant="contained"
          color="primary"
          onClick={switchViewToFavourites}
          startIcon={<PetsIcon />}
        >
          {" "}
          {showFavourites ? "Search Photos" : "Show favourites"}{" "}
        </Button>
        <div style={{ float: "right" }}>
          {" "}
          Number of columns:{" "}
          <ButtonGroup
            variant="text"
            color="primary"
            aria-label="text primary button group"
          >
            <Button
              onClick={() => setNumberOfCols(2)}
              color={numberOfCols === 2 ? "secondary" : "primary"}
            >
              Two
            </Button>
            <Button
              onClick={() => setNumberOfCols(3)}
              color={numberOfCols === 3 ? "secondary" : "primary"}
            >
              Three
            </Button>
            <Button
              onClick={() => setNumberOfCols(5)}
              color={numberOfCols === 5 ? "secondary" : "primary"}
            >
              Five
            </Button>
          </ButtonGroup>
        </div>
      </div>
      {loading ? <Loading /> : <div></div>}
      <div className={classes.root}>
        <div>
          {!showFavourites && (
            <div className={classes.searchBar}>
              <SearchBar />
            </div>
          )}

          <div>
            {textToSearch && !showFavourites && (
              <div style={{ textAlign: "center", height: "50px" }}>
                <Typography variant="body1" component="p">
                  Search results for: {textToSearch} <span> </span>
                  <Button variant="outlined" onClick={onClear}>
                    Clear
                  </Button>
                </Typography>
              </div>
            )}
          </div>
        </div>

        <GridList
          cellHeight={200}
          cols={numberOfCols}
          className={classes.gridList}
        >
          {!showFavourites &&
            pictures.map(pic => (
              <GridListTile
                key={parseInt(pic.id)}
                rows={howManyRows()}
                cols={1}
              >
                <ListItem photo={pic} isLiked={checkIfLiked(pic)} />{" "}
              </GridListTile>
            ))}
          {showFavourites &&
            favPictures.map(pic => (
              <GridListTile
                key={parseInt(pic.id)}
                rows={howManyRows()}
                cols={1}
              >
                <ListItem photo={pic} isLiked={checkIfLiked(pic)} />{" "}
              </GridListTile>
            ))}
        </GridList>
        <div style={{ height: "100px" }}>
          {loading ? <Loading /> : <div></div>}
          {!showFavourites && (
            <Button
              className={classes.loadMore}
              variant="contained"
              color="primary"
              onClick={loadMorePages}
            >
              Load more
            </Button>
          )}{" "}
        </div>
      </div>
      <Dialog
        maxWidth="md"
        aria-labelledby="simple-dialog-title"
        aria-describedby="simple-dialog-description"
        open={singlePhotoView}
        scroll="paper"
        onClose={onXClicked}
      >
        <DialogContent>
          <SinglePhotoContainer
            photo={photoToShow}
            isLiked={checkIfLiked(photoToShow)}
          />
        </DialogContent>
      </Dialog>
      {showFavourites && favPictures.length === 0 ? (
        <div className={classes.noFav}>
          {" "}
          You haven't added any photo to favourites.{" "}
        </div>
      ) : (
        <div> </div>
      )}
    </div>
  );
};
export default List;
