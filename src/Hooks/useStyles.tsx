import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper
    },
    gridList: {
      width: "100%",
      height: "100%",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)"
    },
    titleBar: {
      background:
        "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
        "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
    },
    icon: {
      color: "white"
    },
    dialog: {
      maxWidth: "400%",
      backgroundColor: "blue"
    },
    noFav: {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)"
    },
    loadMore: {
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)"
    },
    searchBar: {
      height: "50px"
    },
    paper: {
      position: "absolute",
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3)
    },
    image: {
      marginTop: "auto",
      marginBottom: "auto",
      marginRight: "auto",
      marginLeft: "auto",

      display: "flex"
    },
    test: {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      maxWidth: "100%",
      maxHeight: "100%"
    },
    card: {
      marginBottom: "15px"
    },
    horizontal: {
      height: "100%",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      position: "absolute"
    },
    vertical: {
      width: "120%",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      position: "absolute"
    }
  })
);

export default useStyles;
