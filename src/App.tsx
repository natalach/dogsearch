import React, { useEffect } from "react";

import ListContainer from "./Containers/ListContainer";
import TopBar from "./Components/TopBar";
import { init } from "./Services/localStorageService";

function App() {
  useEffect(() => {
    init();
  });

  return (
    <div className="font">
      <TopBar />
      <ListContainer />
    </div>
  );
}

export default App;
