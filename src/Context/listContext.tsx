import React, { useState, createContext } from "react";
import Photo from "../Interfaces/Photo";

type ListContextType = {
  singlePhotoView: boolean;
  favPictures: Photo[];
  photoToShow: Photo;
  showFavourites: boolean;
  onSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onLikeClicked: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    photo: Photo
  ) => void;
  onPhotoClicked: (
    event: React.MouseEvent<HTMLImageElement, MouseEvent>,
    photo: Photo
  ) => void;
  onXClicked: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  switchViewToFavourites: () => void;
  onClear: () => void;
  loadMorePages: () => void;
  loading: boolean;
  textToSearch: string;
  pictures: Photo[];
};

export const ListContext = createContext<ListContextType>({
  singlePhotoView: false,
  favPictures: [],
  photoToShow: {} as Photo,
  showFavourites: false,
  onSubmit: e => {
    throw new Error("updateCount() not implemented");
  },
  onChange: e => {
    throw new Error("updateCount() not implemented");
  },
  onLikeClicked: (e, p) => {
    throw new Error("updateCount() not implemented");
  },
  onPhotoClicked: (e, p) => {
    throw new Error("updateCount() not implemented");
  },
  onXClicked: e => {
    throw new Error("updateCount() not implemented");
  },
  switchViewToFavourites: () => {
    throw new Error("updateCount() not implemented");
  },
  onClear: () => {
    throw new Error("updateCount() not implemented");
  },
  loadMorePages: () => {
    throw new Error("updateCount() not implemented");
  },
  loading: true,
  textToSearch: "",
  pictures: []
});
