import React, { useState, useEffect } from "react";
import SinglePhoto from "../Components/SinglePhoto";

import Photo from "../Interfaces/Photo";

interface Props {
  photo: Photo;
  isLiked: boolean;
}

const SinglePhotoContainer: React.FC<Props> = ({ photo, isLiked }) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [photoData, setPhotoData] = useState();

  const detailsUrl =
    "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=" +
    process.env.REACT_APP_FLICKR_API_KEY +
    "&format=json&nojsoncallback=1&photo_id=" +
    photo.id;
  const photoUrl: string =
    "https://farm" +
    photo.farm +
    ".staticflickr.com/" +
    photo.server +
    "/" +
    photo.id +
    "_" +
    photo.secret +
    ".jpg";

  useEffect(() => {
    fetch(detailsUrl)
      .then(response => response.json())
      .then(
        data => {
          setPhotoData(data);
        },
        error => {
          console.log(error);
        }
      );
    setLoading(false);
  }, [detailsUrl]);

  return (
    <SinglePhoto
      photo={photo}
      loading={loading}
      photoData={photoData}
      photoUrl={photoUrl}
      isLiked={isLiked}
    />
  );
};
export default SinglePhotoContainer;
