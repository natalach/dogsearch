import React, { useState, useEffect } from "react";

import List from "../Components/List";
import Photo from "../Interfaces/Photo";
import { ListContext } from "../Context/listContext";

import {
  getFavPhotos,
  updateLocalStorage
} from "../Services/localStorageService";

const ListContainer: React.FC = () => {
  const [pictures, setPictures] = useState<Photo[]>([]);
  const [favPictures, setFavPictures] = useState<Photo[]>(getFavPhotos());
  const [pages, setPages] = useState<number>(1);
  const [text, setText] = useState<string>("");
  const [textToSearch, setTextToSearch] = useState<string>("");
  const [singlePhotoView, setSinglePhotoView] = useState<boolean>(false);
  const [showFavourites, setShowFavourites] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [photoToShow, setPhotoToShow] = useState<Photo>({} as Photo);

  const url =
    "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=" +
    process.env.REACT_APP_FLICKR_API_KEY +
    "&tags=dogs&format=json&nojsoncallback=1&page=" +
    pages +
    "&per_page=100&text=" +
    textToSearch;

  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setPages(1);
    setPictures([]);
    setTextToSearch(text);
  };

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const onLikeClicked = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    photo: Photo
  ) => {
    updateLocalStorage(photo);
    setFavPictures(getFavPhotos());
  };

  const onPhotoClicked = (
    event: React.MouseEvent<HTMLImageElement, MouseEvent>,
    photo: Photo
  ) => {
    setSinglePhotoView(true);
    setPhotoToShow(photo);
  };

  const onXClicked = () => {
    setSinglePhotoView(false);
  };

  const switchViewToFavourites = () => {
    if (showFavourites === false) {
      setFavPictures(getFavPhotos());
    }
    setShowFavourites(!showFavourites);
  };

  const onClear = () => {
    setTextToSearch("");
    setPages(1);
    setPictures([]);
  };

  const loadMorePages = () => {
    setPages(pages => pages + 1);
  };

  useEffect(() => {
    setLoading(true);
    fetch(url)
      .then(response => response.json())
      .then(
        data => {
          const temp2: Photo[] = data.photos.photo.map((pic: Photo) => {
            return pic;
          });
          if (temp2.length === 0) alert("No results for given criteria.");
          setPictures(pictures.concat(temp2));
        },
        error => {
          console.log(error);
        }
      );
    setLoading(false);
  }, [textToSearch, pages, loading, url]);

  return (
    <ListContext.Provider
      value={{
        singlePhotoView: singlePhotoView,
        favPictures: favPictures,
        photoToShow: photoToShow,
        showFavourites: showFavourites,
        onSubmit: onSubmit,
        onChange: onChange,
        onLikeClicked: onLikeClicked,
        onPhotoClicked: onPhotoClicked,
        onXClicked: onXClicked,
        switchViewToFavourites: switchViewToFavourites,
        onClear: onClear,
        loadMorePages: loadMorePages,
        loading: loading,
        textToSearch: textToSearch,
        pictures: pictures
      }}
    >
      <List />
    </ListContext.Provider>
  );
};
export default ListContainer;
